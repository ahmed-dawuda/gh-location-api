<div class="card">
  <div class="card-header alert alert-info">
    <h3>Welcome to WhereInGH Simple API</h3>
  </div>

  <div class="card-block">
  <p class="card-text alert alert-info" style="text-align: center;">
    WhereInGH is a simple api that provides detailed locations all over Ghana
</p>
    <h4 class="card-title">Enter Your Email to get Api token</h4>

    <form method="get" action="{{ route('token') }}" id="loc_form">
      <div class="form-group">
        <input type="text" class="form-control" id="email" placeholder="Email" name="email">
      </div>
      <div class="form-group">
        <button type="submit" id="submit" class="btn btn-outline-info">Get Token</button>
      </div>
    </form>
    <div class="form-group" style="display: none;" id="token_div">
        <input type="text" class="form-control" readonly id="token">
    </div>
  </div>
</div>