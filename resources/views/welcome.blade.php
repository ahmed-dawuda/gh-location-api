<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>WhereInGH Simple API</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nprogress.css') }}">
        <script src="{{ asset('js/nprogress.js') }}" type="text/javascript"></script>
        <style type="text/css">
            body{overflow-x: hidden;}
        </style>
    </head>
    <body>

        <nav class="navbar navbar-toggleable-md navbar-light bg-faded alert alert-info">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <a class="navbar-brand" href="{{ route('home') }}">WhereInGH</a>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{ route('home') }}" id="home">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('docs') }}" id="doc">Doc</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Develop</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About</a>
              </li>
            </ul>
            {{-- <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="text" placeholder="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form> --}}
          </div>
        </nav>
        <br>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" id="mainBody">
                @if(url()->current() == route('home') || url()->current() == route('visit'))
                    @include('home')
                @elseif(url()->current() == route('docs'))
                    @include('docs')
                @endif
            </div>
            <div class="col-md-3"></div>
        </div>
                
        <script type="text/javascript">
            $(document).ready(function(event){
                $(document).on('submit','#loc_form',function(event){
                    event.preventDefault();
                    console.log($(this).serialize())
                    $('#submit').html('<i class="fa fa-spinner fa-spin"></i> Getting token...')
                    $.get($(this).attr('action'),$(this).serialize(),function(result){
                        $('#submit').html('Get Token')
                        console.log(result)
                        if(!result.success){
                            $('#token').val(result.all_errors.email[0])
                        }else{
                            $('#token').val("Your token is: "+result.token)
                        }
                        $('#token_div').show();
                    })
                })

                $(document).on('click','#doc',function(event){
                    event.preventDefault()
                    NProgress.start()
                    // NProgress.inc()
                    $.get($(this).attr('href'),function(result){
                        NProgress.done()
                        $('#mainBody').html(result)
                    })
                })

                $(document).on('click','#home',function(event){
                    event.preventDefault()
                    NProgress.start()
                    // NProgress.inc()
                    $.get($(this).attr('href'),function(result){
                        NProgress.done()
                        $('#mainBody').html(result)
                    })
                })

                $(document).on('click','.try',function(event){
                    event.preventDefault()
                    NProgress.start()
                    $(this).html('<i class="fa fa-spinner fa-spin"></i> Getting response')
                    var link = $(this);
                    // console.log($(this).parent().find('input').val()+'&try=true')
                    $.get($(this).parent().find('input').val()+'&try=true',function(result){
                        console.log(result);
                        link.text('Try')
                        NProgress.done()
                    })
                });
            })
        </script>
    </body>
</html>
