<div class="card">
  <div class="card-header alert alert-info">
    <h3>WhereInGH Simple API Docs</h3>
  </div>

  <div class="card-block">
  <p class="card-text alert alert-warning">
    <i class="fa fa-chevron-right"></i> This is the detail implementation..Its not difficult, so don't freak out<br><br>
    <i class="fa fa-exclamation"></i><span style="color: red;"> If you're using PC then hurray, you can try the code below, just open your browser console to view the response</span><br><br>
    <i class="fa fa-exclamation"></i> Take note that every request requires api_token<br><br>
    <i class="fa fa-exclamation"></i> Take note that all request data must be in lowercase<br><br>
    <i class="fa fa-exclamation"></i> Take note that for regions, use their abbreviation in your url. e.g greater accra = ga, northern = n, volta = n, etc. <br><br>
  </p>

  <p class="card-text">
    <i class="fa fa-code fa-2x"></i> Now is time to code<br><br>
  </p>
<hr>
  <p class="card-text">
     <h5 style="color: red;"><i class="fa fa-chevron-right"></i> To retrieve all regions in Ghana</h5><br>
    <div class="form-group">
        <input type="text" class="form-control"  value="{{ route('region',['api_token'=>'YOUR_TOKEN_HERE']) }}">
        <p class="alert alert-success">This retrieves all regions.<br></p>
        <a href="#" class="btn btn-outline-info try">Try</a>
    </div>

    <i class="fa fa-info"></i> Sometimes you won't want to retrieve all regions, pass a 'not' field, and specify the regions abbreviation<br>
    <div class="form-group">
        e.g. <input type="text" class="form-control"  value="{{ route('region',['api_token'=>'YOUR_TOKEN_HERE','not'=>'ga']) }}">
        
        <p class="alert alert-success">This retrieves all regions but greater accra region. Take note that, you dont have to specify the "region" when stating the region's abbreviation. e.g. greater accra region is simply ga(not gar), volta region is v, not vr etc.<br></p>
        <a href="#" class="btn btn-outline-info try">Try</a>
    </div>
    <i class="fa fa-info"></i> At times, you want to exclude several regions? separate several abbreviations by a ,(comma) character<br>
    <div class="form-group">
        <input type="text" class="form-control"  value="{{ route('region',['api_token'=>'YOUR_TOKEN_HERE']) }}&not=ga,v,n,ba">
        {{-- <br><a href="#" class="btn btn-info">Try</a> --}}
        <p class="alert alert-success">This retrieves all regions but greater accra, volta, northern and brong ahafo regions</p>
        <a href="#" class="btn btn-outline-info try">Try</a>
    </div>
  </p>
<hr>

  <p class="card-text">
  <h5 style="color: red;"><i class="fa fa-chevron-right"></i> To retrieve all districts of a particular region</h5><br>
    {{-- <i class="fa fa-chevron-right"></i> <span style="color: red;"></span><br> --}}
    <i class="fa fa-info"></i> Pass 'r_abbr' field that contains the region's abbreviation.
    <div class="form-group">
        e.g. <input type="text" class="form-control"  value="{{ route('r_districts',['api_token'=>'YOUR_TOKEN_HERE','r_abbr=ga']) }}">
        {{-- <br><a href="#" class="btn btn-info">Try</a> --}}
         <p class="alert alert-success">This returns all districts in greater accra.</p>
         <a href="#" class="btn btn-outline-info try">Try</a>
    </div>
    <i class="fa fa-info"></i> At times, you want to retrieve only districts that are municipal<br>
    Pass a 'and' field and give it the value 'municipal'.
    <div class="form-group">
        <input type="text" class="form-control"  value="{{ route('r_districts',['api_token'=>'YOUR_TOKEN_HERE','r_abbr=ga','and=municipal']) }}">
        {{-- <br><a href="#" class="btn btn-info">Try</a> --}}
        <p class="alert alert-success">This retrieves all districts that are only municipals</p>
        <a href="#" class="btn btn-outline-info try">Try</a>
    </div>
  </p>
<hr>
  
  </div>
</div>