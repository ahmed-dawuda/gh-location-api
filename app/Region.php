<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
	public $timestamps = false;

	public function getCityIDs(){
    	$cities = $this->cities;
    	$cityIDs = array();
    	foreach($cities as $city){
    		$cityIDs[] = $city->id;
    	}
    	return $cityIDs;
    }

    public function getTownIDs(){
    	$towns = $this->towns();
    	$townIDs = array();	
    	foreach($towns as $town){
    		$townIDs[] = $town->id;
    	}
    	return $townIDs;
    }
	
    public function districts(){
    	return $this->hasMany('App\District');
    }

    public function cities(){
    	return $this->hasManyThrough('App\City', 'App\District');
    }

    public function towns(){
    	$towns = App\Town::whereIn('city_id',$this->getCityIDs())->orderBy('tname')->get();
    	return $towns;
    }

    public function streets(){
    	$streets = App\Street::whereIn('town_id',$this->getTownIDs())->orderBy('sname')->get();
    	return $streets;
    }


}
