<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
	public $timestamps = false;
	
    public function city(){
    	return $this->belongsTo('App\City');
    }

    public function areas(){
    	return $this->hasMany('App\Area');
    }
}
