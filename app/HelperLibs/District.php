<?php
namespace App\HelperLibs;
use App\District as SingleDistrict;

	class District{
		public $towns_uri;
		public $cities_uri;
		public $streets_uri;
		public $name;

		function __construct(SingleDistrict $district,$api_token){
			$this->name = $district->dname." District";
			$this->cities_uri = route('d_cities',['api_token'=>$api_token,'district'=>$district->dname]);
			$this->towns_uri = route('d_towns',['api_token'=>$api_token,'district'=>$district->dname]);
			$this->streets_uri = route('d_streets',['api_token'=>$api_token,'district'=>$district->dname]);
		}

		public function getData(){
			return array(
				'name'=>$this->name,
				'url' => array('cities'=>$this->cities_uri,
				'towns'=>$this->towns_uri,
				'streets'=>$this->streets_uri)
				);
		}

	}