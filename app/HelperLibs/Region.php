<?php

	namespace App\HelperLibs;
	use App\Region as SingleRegion;

	class Region{

		public $towns_uri;
		public $cities_uri;
		public $districts_uri;
		public $streets_uri;
		public $region_name;
		public $acronym;

		function __construct(SingleRegion $region,$api_token){
			$names = explode(" ", $region->rname);
			$name = '';
			foreach($names as $n){
				$name .= ucfirst($n)." ";
			}
			$this->acronym = $region->acronym;
			$this->region_name = $name . "Region";
			$this->districts_uri = route('r_districts',['api_token'=>$api_token,'r_abbr'=> $region->acronym]);
			$this->cities_uri = route('r_cities',['api_token'=>$api_token,'r_abbr'=> $region->acronym]);
			$this->streets_uri = route('r_streets',['api_token'=>$api_token,'r_abbr'=> $region->acronym]);
			$this->towns_uri = route('r_towns',['api_token'=>$api_token,'r_abbr'=> $region->acronym]);
		}

		public function getData(){
			return array(
				'region' => $this->region_name,
				'url' => array('districts'=> $this->districts_uri,
				'cities'=> $this->cities_uri,
				'towns'=> $this->towns_uri,
				'streets'=> $this->streets_uri,),
				'region_abbr'=>$this->acronym,
				
				);
		}


	}