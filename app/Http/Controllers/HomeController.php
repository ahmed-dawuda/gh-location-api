<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use DB;
use Validator;

class HomeController extends Controller
{
    public function sendMail(Request $request){
    	// return $request->all();

    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return \Response::json(array(
		        'success' => false,
		        'all_errors' => $validator->getMessageBag()->toArray()
		    ), 200);
        }
        $user = User::where('email',$request->email)->get()->first();
       // return var_dump($user->api_token);
        $api_token = '';
        if(is_null($user)){
        	$api_token = $this->randomKey(7);
	        $api_holder = new User();
	        $api_holder->email = $request->email;
	        $api_holder->api_token = $api_token;
	        $api_holder->save();
	        // return "is null";
        }else{
        	$api_token = $user->api_token;
        	// return "not null";
        }

        return \Response::json(array(
		        'success' => true,
		        'token' => $api_token,
		    ), 200);
    }

    public function randomKey($length) {
	    $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
	    $key = '';
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}

	public function docs(Request $request){
		if($request->ajax()){
			return view('docs');
		}
		return view('welcome');
	}

	public function home(Request $request){
		if($request->ajax()){
			return view('home');
		}
		return view('welcome');
	}
}
