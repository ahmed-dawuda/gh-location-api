<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Region;
use App\HelperLibs\Region as FinalRegion;
use App\HelperLibs\District as FinalDistrict;

class ApiController extends Controller
{
    public $message;
    public $visit_here;
    public $dev = "?api_token=";
    function __construct(){
        $this->message = "Thank you for using WhereInGH Simple API. Support this project by providing more data. That's it, very simple";
        $this->visit_here = route('dev');
    }

    public function region(Request $request){
    	
    	$regions = $this->requiredRegions($request);

    	$finalregions = array();
    	foreach($regions as $region){
    		$single_region = new FinalRegion($region,$request->api_token);
    		$finalregions[] = $single_region->getData();
    	}
    	
    	return response()->json(
    		['status'=>'success','message'=>$this->message,'feedback'=>$this->visit_here.$this->dev.$request->api_token,'regions'=>$finalregions]
    	,200);
    }


    public function requiredRegions($request){
    	if($request->has('not')){
    		$exs = explode(",", $request->not);
    		return Region::whereNotIn('acronym',$exs)->get();
    	}else{
    		return Region::all();
    	}
    }

    public function r_districts(Request $request){

        $region = Region::where('acronym',strtolower($request->r_abbr))->get()->first();
        $districts = $region->districts;
        // $districts = $metaDistricts;
        if($request->has('and') && $request->and == 'municipal'){
            // $districts = $metaDistricts->where('dname','LIKE',$request->but);
            $districts = $region->districts()->where('dname','LIKE','%'.$request->and.'%')->get();
        }
    	// $region = Region::where('acronym',strtolower($request->r_abbr))->get()->first();
        
        $finaldistricts = array();
        foreach($districts as $district){
            $dist = new FinalDistrict($district,$request->api_token);
            $finaldistricts[] = $dist->getData();
        }

        return response()->json(
            ['status'=>'success','message'=>$this->message,'feedback'=>$this->visit_here,'districts'=>$finaldistricts]
        ,200);
    }

    public function r_streets(Request $request){
        // return "hello";
        return response()->json(['status'=>'unavailable','message'=>$this->message,'feedback'=>$this->visit_here,'streets'=>null],503);
    }

    public function r_towns(Request $request){
        return response()->json(['status'=>'unavailable','message'=>$this->message,'feedback'=>$this->visit_here,'towns'=>null],503);
    }

    public function r_cities(Request $request){
        return response()->json(['status'=>'unavailable','message'=>$this->message,'feedback'=>$this->visit_here,'cities'=>null],503);
    }

    public function d_cities(Request $request){
        return response()->json(['status'=>'unavailable','message'=>$this->message,'feedback'=>$this->visit_here,'cities'=>null],503);
    }

    public function d_towns(Request $request){
        return response()->json(['status'=>'unavailable','message'=>$this->message,'feedback'=>$this->visit_here,'towns'=>null],503);
    }

    public function d_streets(Request $request){
        return response()->json(['status'=>'unavailable','message'=>$this->message,'feedback'=>$this->visit_here,'streets'=>null],503);       
    }

    public function dev(Request $request){
        
    }
}
