<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $code1 = 401;
        $code2 = 503;

        if($request->has('try')){
            $code1 = 200;
            $code2 = 200;
        }

        if($request->has('api_token')){

            if(!User::where('api_token',$request->api_token)->get()->count()){
                return response()->json(['status'=>'failed','reason'=>'You are not authorized to use WhereInGH API'],$code1);
            }
            
        }else{
            return response()->json(['status'=>'failed','reason'=>'api_token is required'],$code2);
        }

        return $next($request);
    }
}
