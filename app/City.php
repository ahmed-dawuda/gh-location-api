<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	public $timestamps = false;
	
    public function district(){
    	return $this->belongsTo('App\District');
    }

    public function towns(){
    	return $this->hasMany('App\Town');
    }
}
