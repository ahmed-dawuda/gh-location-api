<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	public $timestamps = false;
	
    public function town(){
    	return $this->belongsTo('App\Town');
    }

    public function streets(){
    	return $this->hasMany('App\Street');
    }
}
