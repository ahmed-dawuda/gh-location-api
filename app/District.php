<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	public $timestamps = false;
	
    public function region(){
    	return $this->belongsTo('App\Region');
    }

    public function cities(){
    	return $this->hasMany('App\City');
    }
}
