<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1.0')->group(function () {

    Route::get('/regions',['as'=>'region','uses'=>'ApiController@region'])->middleware('check_token');
    Route::get('/region/districts',['as'=>'r_districts','uses'=>'ApiController@r_districts'])->middleware('check_token');

    //these section not yet worked on
    Route::get('/region/cities',['as'=>'r_cities','uses'=>'ApiController@r_cities'])->middleware('check_token');
    Route::get('/region/towns',['as'=>'r_towns','uses'=>'ApiController@r_towns'])->middleware('check_token');
    Route::get('/region/streets',['as'=>'r_streets','uses'=>'ApiController@r_streets'])->middleware('check_token');

    Route::get('/district/cities',['as'=>'d_cities','uses'=>'ApiController@d_cities'])->middleware('check_token');
    Route::get('/district/towns',['as'=>'d_towns','uses'=>'ApiController@d_towns'])->middleware('check_token');
    Route::get('/district/streets',['as'=>'d_streets','uses'=>'ApiController@d_streets'])->middleware('check_token');
    
});