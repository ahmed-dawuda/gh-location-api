<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'visit',function () {
    return view('welcome');
}]);

Route::get('/get-token',['as'=>'token','uses'=>'HomeController@sendMail']);
Route::get('/docs',['as'=>'docs','uses'=>'HomeController@docs']);
Route::get('/home',['as'=>'home','uses'=>'HomeController@home']);
Route::get('/dev/api',['as'=>'dev','uses'=>'HomeController@dev'])->middleware('check_token');
