<?php

use Illuminate\Database\Seeder;

class BADistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Asunafo North Municipal',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asunafo South',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asutifi',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asutifi South',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Atebubu-Amantin',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Banda',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Berekum Municipal',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Dormaa East',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Dormaa Municipal',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Dormaa West',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Jaman North',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Jaman South',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kintampo North Municipal',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kintampo South',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nkoranza North',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nkoranza South Municipal',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Pru',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sene',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sene West',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sunyani Municipal',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sunyani West',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tain',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tano North',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tano South',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Techiman Municipal',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Techiman North',
            'region_id' => 4,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Wenchi Municipal',
            'region_id' => 4,
        ]);
    }
}
