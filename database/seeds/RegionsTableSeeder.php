<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            'rname' => 'greater accra',
            'acronym' => 'ga',
        ]);

        DB::table('regions')->insert([
            'rname' => 'ashanti',
            'acronym' => 'a',
        ]);

        DB::table('regions')->insert([
            'rname' => 'volta',
            'acronym' => 'v',
        ]);

        DB::table('regions')->insert([
            'rname' => 'brong-ahafo',
            'acronym' => 'ba',
        ]);

        DB::table('regions')->insert([
            'rname' => 'western',
            'acronym' => 'w',
        ]);

        DB::table('regions')->insert([
            'rname' => 'upper west',
            'acronym' => 'uw',
        ]);

        DB::table('regions')->insert([
            'rname' => 'upper east',
            'acronym' => 'ue',
        ]);

        DB::table('regions')->insert([
            'rname' => 'northern',
            'acronym' => 'n',
        ]);

        DB::table('regions')->insert([
            'rname' => 'eastern',
            'acronym' => 'e',
        ]);

        DB::table('regions')->insert([
            'rname' => 'central',
            'acronym' => 'c',
        ]);
    }
}
