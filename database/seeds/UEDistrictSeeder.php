<?php

use Illuminate\Database\Seeder;

class UEDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Bawku Municipal',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bawku West',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Binduri',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bolgatanga Municipal',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bongo',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Builsa',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Builsa South',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Garu-Tempane',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kassena Nankana East',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kassena Nankana West',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nabdam',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Pusiga',
            'region_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Talensi',
            'region_id' => 7,
        ]);
    }
}
