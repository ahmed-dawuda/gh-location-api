<?php

use Illuminate\Database\Seeder;

class ADistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Adansi North',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Adansi South',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Afigya-Kwabre',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ahafo Ano North',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ahafo Ano South',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Amansie Central',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Amansie West',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asante Akim Central Municipal',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asante Akim North',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asante Akim South',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asokore Mampong Municipal',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Atwima Kwanwoma',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Atwima Mponua',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Atwima Nwabiagya',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bekwai Municipal',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bosome Freho',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Botsomtwe',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ejisu-Juaben Municipal',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ejura - Sekyedumase',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kumasi Metropolitan',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kumawu',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kwabre East',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Mampong Municipal',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Obuasi Municipal',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Offinso North',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Offinso South Municipal',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sekyere Afram Plains',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sekyere Central',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sekyere East',
            'region_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sekyere South',
            'region_id' => 2,
        ]);
    }
}
