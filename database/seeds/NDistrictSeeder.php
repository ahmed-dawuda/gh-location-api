<?php

use Illuminate\Database\Seeder;

class NDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Bole',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bunkpurugu-Yunyoo',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Central Gonja',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Chereponi',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'East Gonja',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'East Mamprusi',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Gushegu',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Karaga',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kpandai',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kumbungu',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Mamprugo Moaduri',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Mion',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nanumba North',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nanumba South',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'North Gonja',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Saboba',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sagnarigu',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Savelugu-Nanton',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sawla-Tuna-Kalba',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tamale Metropolitan',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tatale Sangule',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tolon',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'West Gonja',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'West Mamprusi',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Yendi Municipal',
            'region_id' => 8,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Zabzugu',
            'region_id' => 8,
        ]);

        
    }
}
