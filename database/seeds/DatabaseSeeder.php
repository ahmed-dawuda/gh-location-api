<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionsTableSeeder::class);
        $this->call(GaDistrictSeeder::class);
        $this->call(VDistrictSeeder::class);
        $this->call(BADistrictSeeder::class);
        $this->call(UWDistrictSeeder::class);
        $this->call(UEDistrictSeeder::class);
        $this->call(EDistrictSeeder::class);
        $this->call(NDistrictSeeder::class);
        $this->call(CDistrictSeeder::class);
        $this->call(ADistrictSeeder::class);
        $this->call(WDistrictSeeder::class);
    }
}
