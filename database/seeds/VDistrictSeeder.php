<?php

use Illuminate\Database\Seeder;

class VDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Adaklu',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Afadjato South',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Agotime Ziope',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Akatsi North',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Akatsi South',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Biakoye',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Central Tongu',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ho Municipal',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ho West',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Hohoe Municipal',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Jasikan',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kadjebi',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Keta Municipal',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ketu North',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ketu South Municipal',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kpando Municipal',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Krachi East',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Krachi Nchumuru',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Krachi West',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nkwanta North',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nkwanta South',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'North Dayi',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'North Tongu',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'South Dayi',
            'region_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dname' => 'South Tongu',
            'region_id' => 3,
        ]);
    }
}
