<?php

use Illuminate\Database\Seeder;

class CDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Abura/Asebu/Kwamankese',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Agona East',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Agona West Municipal',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ajumako/Enyan/Essiam',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asikuma/Odoben/Brakwa',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Assin North Municipal',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Assin South',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Awutu-Senya',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Awutu Senya East',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Cape Coast Metropolitan',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Effutu Municipal',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ekumfi',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Gomoa East',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Gomoa West',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Komenda/Edina/Eguafo/Abirem Municipal',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Mfantsiman Municipal',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Twifo-Ati Mokwa',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Twifo/Heman/Lower Denkyira',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Upper Denkyira East Municipal',
            'region_id' => 10,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Upper Denkyira West',
            'region_id' => 10,
        ]);

        
    }
}
