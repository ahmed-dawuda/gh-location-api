<?php

use Illuminate\Database\Seeder;

class UWDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Daffiama Bussie Issa',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Jirapa',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Lambussie Karni',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Lawra',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nadowli',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nandom',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sissala East',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sissala West',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Wa East',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Wa Municipal',
            'region_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Wa West',
            'region_id' => 6,
        ]);
    }
}
