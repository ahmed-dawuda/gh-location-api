<?php

use Illuminate\Database\Seeder;

class GaDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Accra Metropolitan',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ada West',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Adenta Municipal',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ashaiman Municipal',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Dangme East',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Dangme West',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ga Central',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ga East Municipal',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ga South Municipal',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ga West Municipal',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kpone Katamanso',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'La Dade Kotopon Municipal',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'La Nkwantanang Madina',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ledzokuku-Krowor Municipal',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ningo Prampram',
            'region_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tema Metropolitan',
            'region_id' => 1,
        ]);
    }
}
