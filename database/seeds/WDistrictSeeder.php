<?php

use Illuminate\Database\Seeder;

class WDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Ahanta West',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Aowin/Suaman',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bia',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bia East',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bibiani/Anhwiaso/Bekwai',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Bodi',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ellembele',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Jomoro',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Juabeso',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Mpohor',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Mpohor/Wassa East',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Nzema East Municipal',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Prestea-Huni Valley',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sefwi Akontombra',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sefwi-Wiawso',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Sekondi Takoradi Metropolitan',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Shama',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Suaman',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Tarkwa-Nsuaem Municipal',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Wasa Amenfi East',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Wasa Amenfi West',
            'region_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Wassa Amenfi Central',
            'region_id' => 5,
        ]);
    }
}
