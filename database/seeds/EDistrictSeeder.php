<?php

use Illuminate\Database\Seeder;

class EDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'dname' => 'Akuapim South',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Afram Plains South',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Akuapim North',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Akuapim South',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Akyemansa',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Asuogyaman',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Ayensuano',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Atiwa',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Birim Central Municipal',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Birim North',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Birim South',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Denkyembour',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'East Akim Municipal',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Fanteakwa',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kwaebibirem',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kwahu East',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kwahu North',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kwahu South',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Kwahu West Municipal',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Lower Manya Krobo',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'New-Juaben Municipal',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Suhum/Kraboa/Coaltar',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Upper Manya Krobo',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Upper West Akim',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'West Akim Municipal',
            'region_id' => 9,
        ]);

        DB::table('districts')->insert([
            'dname' => 'Yilo Krobo Municipal',
            'region_id' => 9,
        ]);

        
    }
}
